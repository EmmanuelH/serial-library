/**	\file Serial.h
Serial class header file.
Contains the declaration for the serial class and its members.

Author: Emmanuel Harley
Date: 17/10/2014
*/

#ifndef __SERIAL_H__
#define __SERIAL_H__

#include <Windows.h>
#include <string>
#include <vector>

using namespace std;

/// Serial communications class
class Serial
{
public:
	Serial();
	Serial(string port);
	~Serial();
	
	DWORD Open(string);
	DWORD Close();
	DWORD SetConfigure(DCB portConfig);
	DWORD SetConfigure(long baud, char parity, char byteSize, char stopBit);
	DWORD GetConfiguration(DCB &config);
	DWORD Write(char *bytesToSend, DWORD byteCount, DWORD &bytesWritten);
	DWORD Read(char* buffer, DWORD bytesToRead, DWORD &bytesRead);
	
	struct portInfo;
	void GetAvailableComPorts(vector<portInfo> &comPorts);
	void DisplayAvailableComPorts();
private:
	/// The serial handle
	HANDLE serialHandle;
};

#endif //__SERIAL_H__