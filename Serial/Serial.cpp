/**	\file Serial.cpp
	Serial class source file.
	Contains the definitions for the members of the serial class.

	Author: Emmanuel Harley
	Date: 17/10/2014
*/
#include "Serial.h"
using namespace std;

/// Structure to hold portnames and their errors when getting available comPorts
struct Serial::portInfo{
	/// Port name of available com port
	string PortName;
	/// It's current state
	DWORD Error;
};

/**	Default Serial constructor
	Currently does nothing.

*/
Serial::Serial()
{
}

/** Serial constructor
	
	\param port Port to initialize serial class on
*/
Serial::Serial(string port)
{
	DWORD error = Open(port);
}

/// Serial deconstructor
Serial::~Serial()
{
	Close();
}

/** Opens serial port

	\param port The com port to open ex: "COM2"
	\return Returns 0 if successful, error as defined by system error codes
*/
DWORD Serial::Open(string port)
{
	Close();
	DWORD error;
	wstring stemp = std::wstring(port.begin(), port.end());
	LPCWSTR sw = stemp.c_str();
	serialHandle = CreateFile(sw,
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		0,
		NULL);
	error = GetLastError();
	if (error != ERROR_SUCCESS)
	{
		serialHandle = INVALID_HANDLE_VALUE;
		return error;
	}
	
	return error;
}

/**	Closes serial port
	Closes the serial port handle serialHandle.

	\return Returns 0 if successful, error as defined by system error codes
*/
DWORD Serial::Close()
{
	if (!CloseHandle(serialHandle))
	{
		serialHandle = INVALID_HANDLE_VALUE;
		return GetLastError();
	}
	else
		return GetLastError();
}

/** Configure the serial port
	Configures serial port according to the specifications defined by comsettings.

	\param portConfig The configuration of the com port
	\return Returns 0 if successful, error as defined by system error codes
*/
DWORD Serial::SetConfigure(DCB portConfig)
{
	if (serialHandle != INVALID_HANDLE_VALUE)
	{
		if (!SetCommState(serialHandle, &portConfig))
		{
			serialHandle = INVALID_HANDLE_VALUE;
			return GetLastError();
		}
		else
			return GetLastError();
	}
}


/** Configure the serial port
	Configures serial port according to the specifications defined by DCB.

\param baud The baud rate of the com port. Default value 9600.
\param parity The parity of the device. Default value NOPARITY.
\param byteSize The size of the bytes being sent. Default value 8.
\param stopBit The number of stop bits to use. Default value ONESTOPBIT.
\return Returns 0 if successful, error as defined by system error codes
*/
DWORD Serial::SetConfigure(long baud = CBR_9600, char parity = NOPARITY, char byteSize = 8, char stopBit = ONESTOPBIT)
{

	DCB PortDCB;
	// Initialize the DCBlength member. 
	PortDCB.DCBlength = sizeof(DCB);
	// Change the DCB structure settings.
	PortDCB.BaudRate = baud;					// Current baud 
	PortDCB.fBinary = TRUE;						// Binary mode; no EOF check 
	PortDCB.fParity = TRUE;						// Enable parity checking 
	PortDCB.fOutxCtsFlow = FALSE;				// No CTS output flow control 
	PortDCB.fOutxDsrFlow = FALSE;				// No DSR output flow control 
	PortDCB.fDtrControl = DTR_CONTROL_ENABLE;	// DTR flow control type 
	PortDCB.fDsrSensitivity = FALSE;			// DSR sensitivity 
	PortDCB.fTXContinueOnXoff = TRUE;			// XOFF continues Tx 
	PortDCB.fOutX = FALSE;						// No XON/XOFF out flow control 
	PortDCB.fInX = FALSE;						// No XON/XOFF in flow control 
	PortDCB.fErrorChar = FALSE;					// Disable error replacement 
	PortDCB.fNull = FALSE;						// Disable null stripping 
	PortDCB.fRtsControl = RTS_CONTROL_ENABLE;	// RTS flow control 
	PortDCB.fAbortOnError = FALSE;				// Do not abort reads/writes on error
	PortDCB.ByteSize = byteSize;				// Number of bits/byte, 4-8 
	PortDCB.Parity = parity;					// 0-4=no,odd,even,mark,space 
	PortDCB.StopBits = stopBit;					// 0,1,2 = 1, 1.5, 2 

	if (serialHandle != INVALID_HANDLE_VALUE)
	{
		if (!SetCommState(serialHandle, &PortDCB))
		{
			serialHandle = INVALID_HANDLE_VALUE;
			return GetLastError();
		}
		else
			return GetLastError();
	}
}
/** Gets the current configuration of the serial port

	\param portConfig Contains current configuration
	\return Returns 0 if successful, error as defined by system error codes
*/
DWORD Serial::GetConfiguration(DCB &portConfig)
{
	if (serialHandle != INVALID_HANDLE_VALUE)
	{
		if (!GetCommState(serialHandle, &portConfig))
		{
			serialHandle = INVALID_HANDLE_VALUE;
			return GetLastError();
		}
		else
			return GetLastError();
	}
}

/** Writes to serial port
	
	\todo {Add overloaded function to add index parameter for bytesToSend.}

	\param bytesToSend The Array of bytes to send
	\param byteCount The amount of bytes to send
	\param bytesWritten Number of bytes written to serial port

	\return Returns 0 if successful, error as defined by system error codes
*/
DWORD Serial::Write(char *bytesToSend, DWORD byteCount, DWORD &bytesWritten)
{
	if (serialHandle != INVALID_HANDLE_VALUE)
	{
		OVERLAPPED stOverlapped = { 0 };
		if (!WriteFile(serialHandle, bytesToSend, byteCount, &bytesWritten, &stOverlapped))
		{
			serialHandle = INVALID_HANDLE_VALUE;
			return GetLastError();
		}
		else
			return GetLastError();
	}
	else
		return ERROR_INVALID_HANDLE;
}

/**	Reads the buffer on the serial port
	
	\param buffer Bytes read from serial input buffer
	\param bytesToRead Number of bytes to read from buffer
	\param bytesRead Number of bytes successfully read from buffer

	\return Returns 0 if successful, error as defined by system error codes
*/
DWORD Serial::Read(char* buffer, DWORD bytesToRead, DWORD &bytesRead)
{
	if (serialHandle != INVALID_HANDLE_VALUE)
	{
		OVERLAPPED stOverlapped = { 0 };
		if (!ReadFile(serialHandle, buffer, bytesToRead, &bytesRead, &stOverlapped))
		{
			serialHandle = INVALID_HANDLE_VALUE;
			return GetLastError();
		}
		else
			return GetLastError();
	}
	else
		return ERROR_INVALID_HANDLE;
}

/** Get all available ports

	Checks all com ports and their status.
	
	\todo Probably more efficient to use built in functions to check.
	As seen here: http://www.codeproject.com/Articles/14469/Simple-Device-Manager.

	\param comPorts Vector of portInfo to keep a list of available com ports and their status
*/
void Serial::GetAvailableComPorts(vector<portInfo> &comPorts)
{
	string port;
	wstring stemp;
	LPCWSTR sw;
	portInfo portInf;

	HANDLE tempHandle;
	DWORD error;
	
	// Check every port.
	for (int i = 1; i <= 256; i++)
	{
		port = "COM" + to_string(i);
		stemp = std::wstring(port.begin(), port.end());
		sw = stemp.c_str();

		tempHandle = CreateFile(sw,
			GENERIC_READ | GENERIC_WRITE,
			0,
			NULL,
			OPEN_EXISTING,
			0,
			NULL);
		error = GetLastError();
		if (error != ERROR_FILE_NOT_FOUND)
		{
			portInf.PortName = port;
			portInf.Error = error;
			comPorts.push_back(portInf);
		}
	}

	CloseHandle(tempHandle);
}

/** Displays all available com ports

*/
void Serial::DisplayAvailableComPorts()
{
	vector<portInfo> comPorts;
	string status;

	// Get the list of open com ports
	GetAvailableComPorts(comPorts);

	if (comPorts.size() > 0)
	{
		// Create headers
		printf("PORTS:\t| STATUS:\n");
		for (int i = 0; i < comPorts.size(); i++)
		{
			if (comPorts[i].Error == ERROR_SUCCESS)
				status = "OPEN";
			else if (comPorts[i].Error == ERROR_ACCESS_DENIED)
				status = "IN USE";
			else
				status = to_string(comPorts[i].Error);

			printf("%s\t| %s\n", comPorts[i].PortName.c_str(), status.c_str());
		}
	}
	else
		printf("No com ports available.\n");

}